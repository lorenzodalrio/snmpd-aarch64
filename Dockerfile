FROM arm64v8/alpine:3.14

LABEL maintainer Lorenzo Dalrio <lorenzo.dalrio@gmail.com>

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="Docker image to provide the net-snmp daemon" \
      org.label-schema.description="Provides snmpd for UDM-P and other small aarch64 footprint environments without package managers" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/lorenzodalrio/snmpd-aarch64" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"

EXPOSE 161 161/udp

RUN apk add --update --no-cache net-snmp

COPY snmpd.conf /etc/snmp

CMD [ "/usr/sbin/snmpd", "-f", "-Lo", "-c", "/etc/snmp/snmpd.conf" ]
